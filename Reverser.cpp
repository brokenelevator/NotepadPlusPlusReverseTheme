#include<fstream>
#include<string>
#include<cstdio>
#include<experimental/filesystem>

int main(int argc, char** argv)
{
if(argc < 2 || argc > 3)
	{
	printf("Usage: Reverser.exe source [target]\n");
	return -1;
	}
if(!std::experimental::filesystem::exists(argv[1]))
	{
	printf("Error: Source file not found.\n");
	return -1;
	};
std::string targetName;
if(argc == 2) targetName = std::string(argv[1]).append(".reversed");
else if(argc == 3) targetName = argv[2];
if(std::experimental::filesystem::exists(targetName))
	{
	printf("Target file already exists. Overwrite[y/n]? ");
	fflush(stdout);
	switch(getc(stdin))
		{
		case 'y':
		case 'Y':
			break;
		default:
			return -2;
		}
	};
std::fstream source;
std::fstream target;
source.open(argv[1], std::ios::in);
if(!source.is_open())
	{
	printf("Error: Source file failed to open.\n");
	return -1;
	}
target.open(targetName, std::ios::out|std::ios::trunc);
if(!target.is_open())
	{
	printf("Error: Target file failed to open.\n");
	source.close();
	return -1;
	}
while(source.good())
	{
	std::string transportBuffer;
	std::getline(source, transportBuffer);
	std::size_t position = transportBuffer.find("Color");
	while(position != std::string::npos)
		{
		position += 7;
		int hexValue = std::stoi(transportBuffer.substr(position, 6), nullptr, 16);
		hexValue = 0xFFFFFF - hexValue;
		char hexString[7];
		std::sprintf(hexString, "%.6X", hexValue);
		transportBuffer.replace(position, 6, hexString);
		position = transportBuffer.find("Color", position);
		}
	target << transportBuffer << std::endl;
	transportBuffer.erase();
	}
source.close();
target.close();
return 0;
}
