# Notepad++ReverseTheme
One day when I was looking for a nice dark theme for Notepad++ I came up with the idea to just reverse colors of the default theme. I didn't like the result, but this tool remained. Maybe someone will find it useful.

First version was created on March 27th 2016.

### Usage
`Reverser.exe source [target]`

With only source present .reversed will be appended to source name.

### Examples
Default theme and its reverse available in examples folder.

### Build
`g++ Reverser.cpp -o Reverser.exe -lstdc++fs`

### Release
Compiled executable is available in tags section.
